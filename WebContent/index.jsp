<%@ page language="java" import="model.Book" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Book Application</title>
</head>
<body>
<h1>Book application</h1>

<jsp:useBean id="books" scope="session" class="model.CatalogRepository" />

<form action="search.jsp">
Search book by id: <input type="text" name="searchCriteria" /><br/>
<input type="submit" />
</form>

<ul>
	<%
			for(Book book: books.getCatalog().getAll()){
				out.println("<li>book with id: "
							+ book.getId()
							+ "and title: "
							+ book.getTitle()
							+ "</li>");
			}
	%>
</ul>

</body>
</html>