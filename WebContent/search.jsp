<%@ page language="java" import="model.Book" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Insert title here</title>
</head>
<body>
<h1>Book application</h1>

<jsp:useBean id="books" scope="session" class="model.CatalogRepository" />
<%
String par = request.getParameter("searchCriteria");

if(par != null && par != ""){
	Book foundBook = null;
	for(Book book : books.getCatalog().getAll()){
		if(book.getId().equals(par))
			foundBook = book;
	}
	
	if(foundBook != null)
		out.print("Book is found title is " + foundBook.getTitle());
	else
		out.print("Book was not found");
}
%>
</body>
</html>