package model;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;

public class Handler extends org.xml.sax.helpers.DefaultHandler {

	private Catalog catalog = new Catalog();
	private Book currentBook = null;
	private String currentTag = null;

	public Catalog getCatalog() {
		return catalog;
	}

	@Override
	public void startDocument() throws SAXException {
		System.out.println("Parsing is started");
	}

	@Override
	public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
		currentTag = qName;
		if (qName.equals("book")) {
			currentBook = new Book();
			currentBook.setId(attributes.getValue("id").toString());
			catalog.addBook(currentBook);
		}
	}

	@Override
	public void characters(char[] ch, int start, int length) throws SAXException {
		System.out.println(currentTag);
		switch (currentTag) {
		case "author":
			currentBook.setAuthor(new String(ch, start, length));
			break;
		case "title":
			currentBook.setTitle(new String(ch, start, length));
			break;
		case "genre":
			currentBook.setGenre(new String(ch, start, length));
			break;
		case "price":
			currentBook.setPrice(new String(ch, start, length));
			break;
		}
	}

	@Override
	public void endElement(String uri, String localName, String qName) throws SAXException {
		if(qName.equals("book"))
			currentBook = null;
		currentTag = "";
	}

	@Override
	public void endDocument() throws SAXException {
		System.out.println("Parsing was ended");
	}
}
