package model;

import java.util.ArrayList;
import java.util.List;

public class Catalog {
	private ArrayList<Book> books = new ArrayList<>();
	
	
	public void addBook(Book book) {
		books.add(book);
	}
	
	public List<Book> getAll(){
		return books;
	}
}

