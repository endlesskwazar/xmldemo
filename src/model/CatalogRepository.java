package model;

import java.io.File;
import java.io.IOException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.xml.sax.SAXException;

import model.Catalog;

public class CatalogRepository {
	private Catalog catalog = new Catalog();
	
	public Catalog getCatalog() {
		return catalog;
	}
	
	public CatalogRepository() {
		SAXParserFactory factory = SAXParserFactory.newInstance();
		Handler handler = new Handler();
		try {
			SAXParser parser = factory.newSAXParser();
			parser.parse(new File("/home/endlesskwazar/Documents/book.xml"), handler);
			this.catalog = handler.getCatalog();
		} catch (ParserConfigurationException | SAXException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	
}
